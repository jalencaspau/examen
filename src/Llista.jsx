import React from "react";

class Llista extends React.Component {

    render() {
        let i = 1;
        let lis = this.props.llista.map(el => <li key={i++}>{el}</li>);

        return (
            <>
                <ul>

                    {lis}

                </ul>
            </>
        );
    }
}

export default Llista;