import React from "react";

class Paraula extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            paraula: ''
        }
        this.cambia = this.cambia.bind(this);
        this.enviar = this.enviar.bind(this);
    }

    cambia(e) {
        const valor = e.target.value;
        const propietat = e.target.name;
        this.setState({ [propietat]: valor });
    }

    enviar(){
        if(this.state.paraula){
            this.props.novaParaula(this.state.paraula);
        }
        this.setState({paraula: ''});
    }

    render() {
        return (
            <>

                <input value={this.state.palabra} onChange={this.cambia} name="paraula" />
                <button onClick={this.enviar}>Enviar</button>
                <button onClick={this.props.borrar}>Borrar</button>

            </>
        );
    }
}

export default Paraula;