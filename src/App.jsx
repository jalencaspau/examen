import React from "react";
import Llista from './Llista';
import Paraula from './Paraula';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lista: ["a", "b"]
    }

    this.novaParaula = this.novaParaula.bind(this);
    this.borrar = this.borrar.bind(this);

  }

borrar(){
  this.setState({
    llista: ['']
  })
}

  novaParaula(paraula) {
    this.setState({
      llista: [...this.state.llista, paraula]
    })
  }

  render() {
    return (
      <>

        <Paraula novaParaula={this.novaParaula} />
        <Llista llista={this.state.llista} />

      </>
    );
  }
}

export default App;